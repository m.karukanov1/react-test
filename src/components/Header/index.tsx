import './style.scss';
import { useAppDispatch, useAppSelector } from 'hooks';
import { removeAll } from 'store/products';

import { Link } from 'react-router-dom';
import Btn from 'components/Btn';

const Header = () => {
  const dispatch = useAppDispatch();
  
  const products = useAppSelector((state) => state.products);
  const hasProductsQuantity = products.filter(el => el.quantity);
  const totalCount = hasProductsQuantity.reduce((totalCount, el) => totalCount + el.quantity, 0);

  return (
    <header className='header'>
      <div className='container header__wrapper'>
        <Link
          to='/'
          className='header__logo'>
          С  М
          <span>супер магазин</span>
        </Link>
        {hasProductsQuantity.length > 0 &&
          <Btn
            onclick={() => dispatch(removeAll())}
            text={'Очистить корзину'}
            modifier={'clear'} />
        }
        <Link
          to='/cart'
          className='header__cart'>
          <img src='/cart.svg' alt='' title='' />
          {hasProductsQuantity.length > 0 &&
            <span>{totalCount}</span>
          }
        </Link>
      </div>
    </header>
  )
};

export default Header;