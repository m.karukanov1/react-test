import './style.scss';

interface PropsType {
  type?: any;
  onclick?: () => void;
  text: string;
  modifier?: string;
}

const defaultProps = {
  type: 'button',
  text: '',
  modifier: '',
};

const Btn = (props: PropsType) => {
  props = { ...defaultProps, ...props };

  return (
    <button
      onClick={props.onclick}
      className={`btn ${props.modifier ? `btn--${props.modifier}` : ''}`}
      type={props.type}>
      {props.text}
    </button>
  )
};

export default Btn;