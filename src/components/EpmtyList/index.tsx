import './style.scss';
import { Link } from 'react-router-dom';

const EmptyList = () => (
  <div className='empty'>
    Ваша корзина пуста, выберите товар из нашего&nbsp;
    <Link
      to='/products'>
      каталога
    </Link>
  </div>
);

export default EmptyList;