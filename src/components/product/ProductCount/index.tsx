import './style.scss';
import { useAppDispatch } from 'hooks';
import { 
  addProduct,
  deacreseProduct,
  increaseProduct
 } from 'store/products';

import Btn from 'components/Btn';

const Count = ({item, isCart = false, className='' }) => {
  const dispatch = useAppDispatch();

  return (
    <div className={`count ${className ? `count--${className}` : ''}`}>
      {(item.quantity === 0 && !isCart) ? (
        <Btn
          onclick={() => dispatch(addProduct(item))}
          text={'Купить'} />
      ) : (
        <div className='count__group'>
          <button
            onClick={() => dispatch(deacreseProduct(item))}
            className='count__action'
            type='button'>
            -
          </button>
          <span>{item.quantity}</span>
          <button
            onClick={() => dispatch(increaseProduct(item))}
            className='count__action'
            type='button'>
            +
          </button>
        </div>
      )}
    </div>
  );
};

export default Count;