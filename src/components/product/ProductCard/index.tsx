import './style.scss';
import { useAppDispatch } from 'hooks';
import { removeAll } from 'store/products';
import { Fragment } from 'react';
import { Link } from 'react-router-dom';

import Count from 'components/product/ProductCount';

const ProductCard = ({ item, isCart = false }) => {
  const dispatch = useAppDispatch();

  return (
    <div
      className={`product-card ${isCart ? 'product-card--cart' : ''}`}>
      <Link className='product-card__link' to={`/products/${item.id}`}>
        <span className='product-card__img'>
          <img src={item.img} alt='' title='' />
        </span>
        <span className='product-card__name'>{item.name}</span>
        {!isCart && 
          <span className='product-card__sum'>{item.sum} ₽</span>
        }
      </Link>
      <Count
        className={`${isCart ? 'cart' : 'card'}`}
        item={item}
        isCart={isCart} />
      {isCart &&
        <Fragment>
          <span className='product-card__sum'>{item.sum * item.quantity} ₽</span>
          <button
            onClick={() => dispatch(removeAll())}
            className='product-card__remove'
            type='button'>
            <img src='/remove.svg' alt='' title='' />
          </button>
        </Fragment>      
      }
    </div>
  )
};

export default ProductCard;