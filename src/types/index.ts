export interface ProductItem {
  id: number,
  img: string,
  img_detail: string,
  name: string,
  sum: number,
  quantity: number,
  main_page: boolean | false,
  desc: boolean,
}

export interface Props {
  products: ProductItem[];
}