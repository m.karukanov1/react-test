import './style.scss';
import { useState, useMemo } from 'react';
import { useAppSelector } from 'hooks';

import ProductCard from 'components/product/ProductCard';
import Btn from 'components/Btn';

const Products = () => {
  const products = useAppSelector((state) => state.products);
  const [sort, setSort] = useState({ order: 'asc', orderBy: 'id' });
  const [search, setSearch] = useState('');

  const sortIcon = (type) => {
    if (sort.orderBy === type) {
      if (sort.order === 'asc') {
        return '↑'
      }

      return '↓'
    }
    
    return '️'
  };

  const searchProducts = (list, val) => {
    return list.filter((el) => el.name.toLowerCase().includes(val.toLowerCase()));
  };

  const sortProducts = (list, sort) => {
    return list.sort((a, b) => {
      const { order, orderBy } = sort;
      const aLocale = `${a[orderBy]}`;
      const bLocale = `${b[orderBy]}`;
  
      if (order === 'asc') {
        return aLocale.localeCompare(bLocale, 'en', { numeric: b[orderBy] })
      } else {
        return bLocale.localeCompare(aLocale, 'en', { numeric: a[orderBy] })
      }
    })
  };

  const searchedProducts = useMemo(() => searchProducts(products, search), [products, search]);
  const sortedProducts = useMemo(() => sortProducts(searchedProducts, sort), [searchedProducts, sort]);

  const handleSort = (type) => {
    setSort((prevSort) => ({
      order: prevSort.order === 'asc' && prevSort.orderBy === type ? 'desc' : 'asc',
      orderBy: type,
    }))
  };

  const clearAll = () => {
    setSort({ order: 'asc', orderBy: 'id' });
    setSearch('');
  };

  const setSortText = (text, type) => {
    return `по ${text} ${sortIcon(type)}`
  };

  return (
    <div className='products'>
      <div className='products__filter'>
        Фильтры:
        <Btn
          onclick={() => handleSort('sum')}
          text={setSortText('цене', 'sum')}
          modifier='sort' />
        <Btn
          onclick={() => handleSort('name')}
          text={setSortText('названию', 'name')}
          modifier='sort' />
        <input
          onChange={(event) => setSearch(event.target.value)}
          value={search}
          type='search'
          className='products__search'
          placeholder='Найти товар по названию' />
        {(sort.orderBy !== 'id' || search) &&
          <Btn
            onclick={() => clearAll()}
            text='очистить фильтры'
            modifier='clear' />
        }
      </div>
      <ul>
        {sortedProducts.map((item) => (
          <li key={item.id}>
            <ProductCard item={item}/>
          </li>
        ))}
      </ul>
    </div>
  )
};

export default Products;