import './style.scss';

import ProductCard from 'components/product/ProductCard';
import EmptyList from 'components/EpmtyList';

const List = ({products}) => (
  <div className='list'>
    {products.length > 0 ? (
      <ul>
        {products.map((item) => (
          <li key={item.id}>
            <ProductCard
              item={item}
              isCart={true} />
          </li>
        ))}
      </ul>
    ) : (
      <EmptyList />
    )}
  </div>
);

export default List;