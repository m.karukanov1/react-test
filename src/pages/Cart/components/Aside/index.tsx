import './style.scss';
import { Link } from 'react-router-dom';

const Aside = ({products}) => {
  const totalSum = products.reduce((totalSum, el) => totalSum + el.sum * el.quantity, 0);
  const totalCount = products.reduce((totalCount, el) => totalCount + el.quantity, 0);

  return (
    <div className='aside'>
      <div className='aside__title'>
        <img src='/cart.svg' alt='' title='' />
        Ваш заказ
      </div>
      <div className='aside__count'>
        <span>{ totalCount } товаров</span>
        { totalSum }  ₽
      </div>
      <div className='aside__sum'>
        <span>Итого</span>
        { totalSum } ₽
      </div>
      <Link
        to='/order'
        className='aside__confirm'>
        оформить заказ
      </Link>
    </div>
  )
};

export default Aside;