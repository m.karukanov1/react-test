import './style.scss';
import { useAppSelector } from 'hooks';

import List from './components/List';
import Aside from './components/Aside';


const Cart = () => {
  const products = useAppSelector((state) => state.products);
  const hasProductsQuantity = products.filter(el => el.quantity);

  return (
    <div className='cart'>
      <List products={hasProductsQuantity} />
      {hasProductsQuantity.length > 0 &&
        <Aside products={hasProductsQuantity} />
      }
    </div>
  )
};

export default Cart;