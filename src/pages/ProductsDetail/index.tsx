import './style.scss';
import { useAppSelector } from 'hooks';
import {
  Navigate,
  useParams 
} from 'react-router-dom';

import Count from 'components/product/ProductCount';

const ProductsDetail = () => {
  const location = useParams();
  const products = useAppSelector((state) => state.products);
  const product = products.find(el => String(el.id) === location.id);
  
  if (!product) {
    return <Navigate to='/404' />
  }

  return (
    <div className='product-detail'>
      <h1>{ product.name }</h1>
      <div className='product-detail__group'>
        <div className='product-detail__img'>
          <img src={product.img_detail} alt='' title='' />
        </div>
        <div className='product-detail__panel'>
          <div className='product-detail__desc'>{product.desc}</div>
          <Count item={product} />
        </div>
      </div>
    </div>
  )
};

export default ProductsDetail;