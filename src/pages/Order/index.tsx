import './style.scss';
import { useAppSelector, useAppDispatch } from 'hooks';
import { Fragment, useEffect } from 'react';
import { clearSend } from 'store/order';
import { Link } from 'react-router-dom';

import EmptyList from 'components/EpmtyList';
import FirstForm from './components/FirstForm';
import SecondForm from './components/SecondForm';
import ThirdForm from './components/ThirdForm';
import Steps from './components/Steps';

const Order = () => {
  const dispatch = useAppDispatch();
  const products = useAppSelector((state) => state.products);
  const step = useAppSelector((state) => state.order.step);
  const send = useAppSelector((state) => state.order.send);
  const hasProductsQuantity = products.filter(el => el.quantity);
  const steps = [
    {
      component: FirstForm,
      title: 'Личные данные',
      type: 'info',
    },
    {
      component: SecondForm,
      title: 'Оплата',
      type: 'card',
    },
    {
      component: ThirdForm,
      title: 'Адрес',
      type: 'address',
    },
  ];

  useEffect(() => {
    return () => {
      if (send) {
        dispatch(clearSend());
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])

  const DynamicComponent = steps[step].component;
  const type = steps[step].type;

  return (
    <div className='order'>
      {(hasProductsQuantity.length > 0 || send) ? (
        <Fragment>
          {send ? (
            <div className='order__confirm'>
              <div>Спасибо, Ваш заказ оформлен!</div>
              <Link
                to='/products'>
                Продолжить покупки
              </Link>
            </div>
          ) : (
            <div className='order__wrap'>
              <Steps steps={steps} />
              <DynamicComponent steps={steps} type={type} />
            </div>
          )}
        </Fragment>
      ) : (
        <EmptyList />
      )}
    </div>
  )
};

export default Order;