import './style.scss';
import { useAppDispatch, useAppSelector } from 'hooks';
import { setStep } from 'store/order';

const Steps = ({steps}) => {
  const dispatch = useAppDispatch();
  const form = useAppSelector((state) => state.order.form);
  const step = useAppSelector((state) => state.order.step);
  const errors = useAppSelector((state) => state.order.errors);

  return (
    <div className='steps'>
      
      <ul className='steps__list'>
        {steps.map((item, index) => (
          <li key={item.title}>
            <button
              onClick={() => dispatch(setStep(index))}
              className={step === index ? 'active': ''}
              type="button">
              {(errors && !form[item.type]) &&
                <span className='steps__error'>!</span>
              }
              <span>{ index + 1 }.</span>
              { item.title }
            </button>
          </li>
        ))}
      </ul>
    </div>
  )
};

export default Steps;