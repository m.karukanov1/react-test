import './style.scss';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useAppDispatch, useAppSelector } from 'hooks';
import { increaseStep, setDataOrder } from 'store/order';

import Buttons from '../Buttons';
import CreditCard from '../CreditCard';

const SecondForm = ({type}) => {
  const dispatch = useAppDispatch();
  const form = useAppSelector((state) => state.order.form);
  const formData = form[type];
  
  const {
    register,
    handleSubmit,
    reset,
    watch,
    formState: { errors },
  } = useForm({
    defaultValues: { 
      name: '',
      number: '',
      month: '',
      year: '',
      cvv: ''
    }
  });

  const onSubmit = (data) => {
    if (data) {
      dispatch(setDataOrder({
        name: type,
        data: data,
      }));
      dispatch(increaseStep());
    }
  };

  useEffect(() => {
    if (formData) {
      reset(formData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <form onSubmit={handleSubmit(onSubmit)} className='form'>
      <div className='form__wrap'>
        <CreditCard cardInfo={watch()} />
        <div className='form__group'>
          <div className='form__group'>
            <label htmlFor='name'>Держатель карты</label>
            <input
              {...register('name', { required: true })}
              id='name'
              placeholder='Введите имя' />
            {errors.name && <span>введите имя</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='number'>Номер карты</label>
            <input
              {...register('number', {
                required: true,
                minLength: 16,
                maxLength: 16,
              })}
              maxLength={16}
              id='number'
              placeholder='**** **** **** ****' />
            {errors.number && <span>неверный номер карты</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='month'>Срок действия карты(месяц)</label>
            <input
              {...register('month', {
                required: true,
                minLength: 2,
                maxLength: 2,
              })}
              maxLength={2}
              id='month'
              placeholder='введите месяц' />
            {errors.month && <span>неверный срок действия карты</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='year'>Срок действия карты(год)</label>
            <input
              {...register('year', {
                required: true,
                minLength: 2,
                maxLength: 2,
              })}
              maxLength={2}
              id='year'
              placeholder='введите год' />
            {errors.year && <span>неверный срок действия карты</span>}
          </div>
          <div className='form__group'>
            <label htmlFor='cvv'>CVV</label>
            <input
              {...register('cvv', {
                required: true,
                minLength: 3,
                maxLength: 3,
              })}
              maxLength={3}
              id='cvv'
              placeholder='введите сvv' />
            {errors.cvv && <span>неверный cvv</span>}
          </div>
        </div>
      </div>
      <Buttons />
    </form>
  )
};

export default SecondForm;
