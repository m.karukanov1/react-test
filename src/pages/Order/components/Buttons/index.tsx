import './style.scss';
import { useAppSelector, useAppDispatch } from 'hooks';
import { deacreseStep } from 'store/order';

import Btn from 'components/Btn';

const Buttons = ({final = false}) => {
  const step = useAppSelector((state) => state.order.step);
  const dispatch = useAppDispatch();

  return (
    <div className='buttons'>
      {step > 0 &&
        <Btn
        onclick={() =>  dispatch(deacreseStep())}
        text={'Назад'} />
      }
      <Btn
        onclick={() => false}
        text={final ? 'Оформить заказ' : 'Далее'}
        type={'submit'} />        
    </div>
  )
};

export default Buttons;