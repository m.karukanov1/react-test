import './style.scss';

const CreditCard = ({cardInfo}) => {
  const getDisplayCardNumber = (number) => {
    const placeholder = "****************";
    const newPlaceholder = placeholder.substr(number.length);
  
    return number.concat("", newPlaceholder).match(/.{1,4}/g);
  };

  const cardNumber = getDisplayCardNumber(cardInfo.number);
  const cardName = cardInfo.name < 1 ? "Name" : cardInfo.name;
  const expiry =
    cardInfo.month < 1 && cardInfo.year < 1
      ? "00/00"
      : `${cardInfo.month}/${cardInfo.year}`;

  return (
    <div className="card">
      <div className="card__number">
        <span className="card__section">{cardNumber[0]}</span>
        <span className="card__section">{cardNumber[1]}</span>
        <span className="card__section">{cardNumber[2]}</span>
        <span className="card__section">{cardNumber[3]}</span>
        {cardNumber[4] && (
          <span className="card__section">{cardNumber[4]}</span>
        )}
      </div>
      <div className="card__info">
        <div className="card__name">
          <span>Имя Фамилия</span>
          <p>{cardName}</p>
        </div>
        <div className="card__expiry">
          <span>Дата</span>
          <p>{expiry}</p>
        </div>
      </div>
    </div>
  );
};

export default CreditCard;