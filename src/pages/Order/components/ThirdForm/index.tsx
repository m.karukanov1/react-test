import './style.scss';
import { useState } from 'react';
import { useEffect } from 'react'
import { YMaps, Map, Placemark } from '@pbe/react-yandex-maps';
import { useAppDispatch, useAppSelector } from 'hooks';
import { 
  setDataOrder, 
  sendForm,
  setErrors
} from 'store/order';
import { removeAll } from 'store/products';

import Buttons from '../Buttons';

const ThirdForm = ({type, steps}) => {
  const dispatch = useAppDispatch();
  const form = useAppSelector((state) => state.order.form);
  const formData = form[type];

  useEffect(() => {
    if (formData) {
      setCoord(formData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const mapState = {
    center: [55.76, 37.64],
    zoom: 9,
    controls: [],
  };
  const [coord, setCoord] = useState([]);

  const getCoord = (e) => {
    const coords = e.get('coords');
    setCoord(coords);
  };

  const onSubmit = (e) => {
    e.preventDefault();

    if (coord.length) {
      dispatch(setDataOrder({
        name: type,
        data: coord,
      }));
    }
  
    const confirmOrder = () => {
      let hasError = false;

      steps.forEach(el => {
        const type = el.type;

        if (!form[type]) {
          hasError = true;
        }
      })

      if (hasError) {
        dispatch(setErrors(true));
      } else {
        dispatch(sendForm());
        dispatch(removeAll());
      }
    };

    confirmOrder();
  };

  return (
    <form onSubmit={onSubmit} className='address'>
      <div className='address__map'>
        <YMaps>
          <Map
            onClick={getCoord}
            defaultState={mapState}
            width='100%'
            height='100%'>
            <Placemark geometry={coord} />
          </Map>
        </YMaps>
      </div>
      <Buttons final={true} />
    </form>
  )
};

export default ThirdForm;