import './style.scss';
import { useEffect } from 'react';
import { useForm, Controller } from 'react-hook-form';
import PhoneInput from 'react-phone-input-2';
import { useAppDispatch, useAppSelector } from 'hooks';
import { increaseStep, setDataOrder } from 'store/order';

import Buttons from '../Buttons';

const FirstForm = ({type}) => {
  const dispatch = useAppDispatch();
  const form = useAppSelector((state) => state.order.form);
  const formData = form[type];

  const {
    register,
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    if (data) {
      dispatch(setDataOrder({
        name: type,
        data: data,
      }));
      dispatch(increaseStep());
    }
  };
  
  useEffect(() => {
    if (formData) {
      reset(formData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <form onSubmit={handleSubmit(onSubmit)} className='form'>
      <div className='form__group'>
        <label htmlFor='name'>Имя</label>
        <input
          {...register('name', { required: true })}
          id='name'
          placeholder='Введите имя' />
        {errors.name && <span>введите имя</span>}
      </div>
      <div className='form__group'>
        <label htmlFor='email'>Электронная почта</label>
        <input
          {...register('email', {
            required: true,
            pattern: /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ 
          })}
          id='email'
          placeholder='Введите email' />
        {errors.email && <span>некорректный email</span>}
      </div>
      <div className='form__group'>
        <label htmlFor='phone'>Номер телефона</label>
        <Controller
          control={control}
          name='phone'
          rules={{
            required: { value: true, message: 'ведите номер телефона' },
            minLength: { value: 11, message: 'некорректный номер' },
            maxLength: { value: 11, message: 'некорректный номер' },
          }}
          render={({ field }) => (
            <PhoneInput
              {...field}
              inputProps={{
                id: 'phone',
              }}
              placeholder={'Введите номер телефона'}
              country={'ru'}
              countryCodeEditable={false} />
          )}
        />
        {errors.phone && <span>{`${errors.phone.message}`}</span>}
      </div>
      <Buttons />
    </form>
  )
};

export default FirstForm;