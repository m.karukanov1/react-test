import './style.scss';

const Advantage = () => (
  <div className='advantage'>
    <h2>Наши преимущества</h2>
    <ul>
      <li>
        <span>Надёжность</span>
        Большой выбор сертифицированных товаров
      </li>
      <li>
        <span>Удобство</span>
        Заберите заказ в любом магазине
      </li>
      <li>
        <span>Экономия</span>
        Экономьте до 70% покупая у нас
      </li>
    </ul>
  </div>
);

export default Advantage;