import './style.scss';
import { Link } from 'react-router-dom';

const Banner = () => (
  <div className='banner'>
    <Link to='/products/1'>
      <img src='/banner.png' alt='' title='' />
    </Link>
  </div>
);

export default Banner;