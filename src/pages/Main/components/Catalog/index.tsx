import './style.scss';
import { useAppSelector } from 'hooks';

import { Link } from 'react-router-dom';
import ProductCard from 'components/product/ProductCard';

const Catalog = () => {
  const products = useAppSelector((state) => state.products);
  
  return (
    <div className='catalog'>
      <h2>Каталог товаров</h2>
      <ul>
      {products
        .filter(product => product.main_page)
        .map((item) => (
          <li key={item.id}>
            <ProductCard item={item}/>
          </li>
        ))
      }
      </ul>
      <Link
        to='/products'
        className='catalog__link'>
        Посмотреть все товары
      </Link>
    </div>
  )
};

export default Catalog;