import Banner from './components/Banner';
import Advantage from './components/Advantage';
import Catalog from './components/Catalog';

const Main = () => (
  <div>
    <Banner />
    <Advantage />
    <Catalog />
  </div>
);

export default Main;