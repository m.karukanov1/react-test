import './style.scss';
import { Link } from 'react-router-dom';

const NotFound = () => (
  <div className='not-found'>
    <div className='not-found__title'>Ошибка 404</div>
    <div className='not-found__description'>Тут ничего нет</div>
    <div className='not-found__text'>Попробуйте вернуться назад или поищите что-нибудь другое</div>
    <Link
      to='/'
      className='not-found__link'>
      На главную
    </Link>
  </div>
);

export default NotFound;