import {
  Routes,
  Route,
  Navigate,
} from 'react-router-dom';

import Header from 'components/Header';
import Footer from 'components/Footer';

import NotFound from 'pages/NotFound';
import Main from 'pages/Main';
import Products from 'pages/Products';
import ProductsDetail from 'pages/ProductsDetail';
import Cart from 'pages/Cart';
import Order from 'pages/Order';

function App() {
  return (
    <div className="App">
      <Header />
      <div className="container container--app">
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="products" element={<Products />} />
          <Route path="products/:id" element={<ProductsDetail />} />
          <Route path="cart" element={<Cart />} />
          <Route path="order" element={<Order />} />
          <Route path="/404" element={ <NotFound /> } />
          <Route path="*" element={ <Navigate to="/404" replace />} />
        </Routes>
      </div>
      <Footer />
    </div>
  );
}

export default App;
