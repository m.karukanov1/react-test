import type { RootState } from 'store'
import { createSlice } from '@reduxjs/toolkit';
import { PRODUCTS_INITIAL_STATE } from './state';

const products = createSlice({
    name: 'products',
    initialState: PRODUCTS_INITIAL_STATE,
    reducers: {
      addProduct: (state, { payload }) => {
        return state.map(el => {
          if (el.id !== payload.id) {
            return el
          }
          return {
            ...el,
            quantity: 1,
          }
        })
      },
      removeProduct: (state, { payload }) => {
          return state.map(el => {
            if (el.id !== payload.id) {
              return el
            }
            return {
              ...el,
              quantity: 0,
            }
          })
      },
      removeAll: (state) => {
        return state.map(el => {
          return {
            ...el,
            quantity: 0,
          }
        })
      },
      increaseProduct: (state, { payload }) => {
        return state.map(el => {
          if (el.id !== payload.id) {
            return el
          }
          return {
            ...el,
            quantity: el.quantity + 1,
          }
        })
      },
      deacreseProduct: (state, { payload }) => {
        return state.map(el => {
          if (el.id !== payload.id) {
            return el
          }
          return {
            ...el,
            quantity: el.quantity - 1,
          }
        })
      },
  }
});

export const {
  addProduct,
  removeProduct,
  removeAll,
  increaseProduct,
  deacreseProduct 
} = products.actions

export const selectCount = (state: RootState) => state.products;
export default products.reducer;