import type { RootState } from 'store'
import { createSlice, current } from '@reduxjs/toolkit';

const initialState = {
  step: 0,
  form: {},
  send: false,
  errors: false,
};

const order = createSlice({
  name: 'order',
  initialState: initialState,
  reducers: {
    increaseStep: (state) => {
      state.step++;
    },
    deacreseStep: (state) => {
      state.step--;
    },
    setStep: (state, {payload}) => {
      state.step = payload;
    },
    setDataOrder: (state, {payload}) => {
      state.form[payload.name] = payload.data;
    },
    resetOrder: (state) => {
      state.step = 0;
      state.form = {};
    },
    sendForm: (state) => {
      console.log(current(state.form));
      state.send = true;
      state.form = {};
      state.errors = false;
      state.step = 0;
    },
    clearSend: (state) => {
      state.send = false;
    },
    setErrors: (state, {payload}) => {
      state.errors = payload;
    },
  }
});

export const {
  increaseStep,
  deacreseStep,
  resetOrder,
  setDataOrder,
  sendForm,
  clearSend,
  setStep,
  setErrors,
} = order.actions

export const selectCount = (state: RootState) => state.order;
export default order.reducer;